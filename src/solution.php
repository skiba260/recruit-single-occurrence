<?php

/**
 * Znajduje liczby, które się nie powtarzają
 *
 * @param $input array Tablica liczb
 * @return array
 */
function findSingle(array $input): array
{
    $inputWithoutDuplicates = array_unique($input);
    $singleNumbers          = [];

    foreach($input as $inputKey => $inputValue) {
        if (!is_string($inputValue)) {
            $input[(string)$inputKey] = (string) $inputValue;
        }
    }

    $inputCounterValues = array_count_values($input);

    foreach($inputWithoutDuplicates as $inputWithoutDuplicatesKey => $inputWithoutDuplicatesValue) {
        if ($inputCounterValues[(string)$inputWithoutDuplicatesValue] > 1) {
            unset($inputWithoutDuplicates[$inputWithoutDuplicatesKey]);
        } else {
            $singleNumbers[] = (string)$inputWithoutDuplicatesValue;
        }
    }

    return $singleNumbers;
}

print_r(findSingle([1, 2, 3, 4, 1, 2, 3]));


print_r(findSingle([11, 21, 33.4, 18, 21, 33.39999, 33.4]));


